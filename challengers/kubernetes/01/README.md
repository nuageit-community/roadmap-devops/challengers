# Implantação do Nginx em um Cluster Kubernetes Local com o Kind e o Helm

## ➤ Introduction

Olá estagiário de DevOps,

Nós temos um desafio para você que visa testar suas habilidades em trabalhar com tecnologias de orquestração de contêineres. Neste desafio, você será responsável por criar um cluster Kubernetes localmente usando o Kind, criar um catálogo Helm para implantar uma aplicação nginx, aplicar o catálogo ao cluster e validar o funcionamento do nginx localmente.

## ➤ Pré Requisitos

Antes de começar o desafio, é importante lembrar que você precisará atender aos seguintes pré-requisitos:

- Familiaridade com a configuração do ambiente de desenvolvimento em sua máquina local.
- Conhecimento básico do Kubernetes e do gerenciador de pacotes Helm.
- Conhecimento básico do nginx e sua configuração.

## ➤ Steps

Para começar, você precisará instalar e configurar o Kind em sua máquina local. Em seguida, crie um cluster Kubernetes usando o Kind e certifique-se de que ele está em execução. Uma vez que o cluster está funcionando, você pode instalar e configurar o Helm para funcionar com o cluster.

Agora que o Helm está configurado, você deve criar um catálogo Helm para o aplicativo nginx. O catálogo deve incluir o código-fonte do nginx, bem como todas as dependências e configurações necessárias para que o aplicativo possa ser implantado no Kubernetes com sucesso.

Depois de criar o catálogo Helm, você deve implantá-lo no cluster Kubernetes usando o Helm e verificar se o aplicativo nginx foi implantado corretamente no cluster. Para validar o funcionamento do nginx, você pode usar o comando kubectl para verificar se o pod está em execução e se o serviço está acessível localmente.

# ➤ Aprendizados

Ao concluir este desafio, você terá aprendido a criar e configurar um cluster Kubernetes localmente usando o Kind, bem como a implantar e gerenciar aplicativos no Kubernetes usando o gerenciador de pacotes Helm. Você também terá aprendido como usar o kubectl para verificar o estado do cluster e de seus aplicativos implantados.

## ➤ Conclusão

Este desafio é uma excelente oportunidade para testar suas habilidades de pensamento crítico, resolução de problemas e capacidade de trabalhar com tecnologias modernas de infraestrutura. Documente cuidadosamente todos os passos que seguir durante o desafio para que possamos entender seu processo de pensamento e sua metodologia de trabalho.

Boa sorte e esperamos ver excelentes resultados de seus esforços!

Atenciosamente,
A equipe de DevOps.
