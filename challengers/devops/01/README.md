# Desafio DevOps

Envie a URL do seu repositório para o email lpmatos@nuageit.com.br.

## ➤ Introduction

Olá candidato(a),

Neste desafio, queremos testar suas habilidades em tecnologias de DevOps, como orquestração de contêineres, infraestrutura como código, observabilidade, pipelines no GitLab e GitOps - elementos essenciais para quem atua nessa área. Sua missão será:

- Realize o setup da infraestrutura Kubernetes localmente, utilizando o Terraform para provisionar todos os recursos necessários.
- Crie uma pipeline no GitLab para o projeto, garantindo que ela execute as etapas de build (docker), lint do Dockerfile(hadolint) e scan da imagem buildada (trivy), proporcionando uma entrega de código mais segura e confiável.
- Implemente a aplicação [podinfo](https://github.com/stefanprodan/podinfo) no ambiente de Kubernetes usando a abordagem GitOps, garantindo que qualquer alteração no código seja refletida automaticamente no ambiente de produção.
- Suba as ferramentas de observabilidade (por exemplo, Prometheus, Grafana, Jaeger, etc.) no ambiente de Kubernetes e configure-as adequadamente para monitorar e coletar métricas, permitindo uma visão detalhada do sistema em execução.
- Detalhe todas as informações e comprove suas entregas com screenshots evidenciando o funcionamento dos diferentes passos do desafio.

Este desafio permitirá avaliarmos sua expertise em DevOps, bem como suas habilidades práticas na criação de ambientes de infraestrutura e automação de processos para garantir uma entrega de software eficiente e confiável. Estamos ansiosos para ver suas soluções e a forma como enfrenta esse cenário complexo.

Boa sorte e mãos à obra!

## ➤ Features

### 1. GitLab

```
-> pipeline com build da imagem do app
    -> [plus] linter do Dockerfile (tip: use o https://github.com/hadolint/hadolint)
      -> falhar se houver severidade >= Warning
-> [plus] scan da imagem usando Trivy (https://github.com/aquasecurity/trivy) standalone (binário)
    -> falhar se houver bug crítico
```

Tips:

- Instale o [GitLab CI/CD workflow agent](https://docs.gitlab.com/ee/user/clusters/agent/#gitlab-cicd-workflow) para fazer o build e aplicar os manifestos no cluster (caso não use fluxcd): https://docs.gitlab.com/ee/user/clusters/agent/install/
- Para fazer o build da imagem, utilize como base o pipeline em https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml
- Utilize o seguinte nome para a imagem: `$CI_REGISTRY/$SEU_USER_GITLAB/podinfo:$CI_COMMIT_SHORT_SHA`

### 2. Terraform

```
-> criar cluster kind
-> [plus] criar repo no gitlab
```

### 3. Kubernetes

```
-> implementar no app
    -> probes liveness e readiness
    -> definir resource de cpu e memória
-> [plus] escalar app com base na métrica `requests_total`
    -> escalar quando observar > 2 req/seg.
-> [plus] instalar app com argocd
```

### 4. Observability

```
-> prometheus stack (prometheus, grafana, alertmanager)
-> retenção de métricas 3 dias
    -> storage local (disco), persistente
-> enviar alertas para um canal no slack
-> logs centralizados (loki, no mesmo grafana do monitoramento)
-> [plus] monitorar métricas do app `request_duration_seconds`
    -> alertar quando observar > 3 seg.
-> [plus] tracing (open telemetry)
```


## ➤ Troubleshooting

Se você tiver algum problema, entre em contato com a equipe de **DevOps** a partir do e-mail lpmatos@nuageit.com.br.

## ➤ Show your support

<div align="center">

Dê um ⭐️ se esse projeto te ajudou!

<img alt="star-wars" src="https://www.icegif.com/wp-content/uploads/baby-yoda-bye-bye-icegif.gif" width="225"/>

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)

</div>
