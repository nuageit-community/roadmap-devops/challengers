<!-- BEGIN_DOCS -->
[❮ Back](README.md)

<div align="center">

<a name="readme-top"></a>

<img alt="nuageit-contributing" src="https://gitlab.com/nuageit-community/images-toolbox/-/raw/main/.gitlab/assets/nuageit-contributing.png" width="225"/>

<h3>All the guidelines for contributing to this project are described in this document</h3>.

</div>

# Summary

[[_TOC_]]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Best practices

>
> - If you're unable to continue with an assigned task, inform the **DevOps Team** promptly.
> - While Generative AI can be useful, minimize it's use for **direct team communication**. We value concise, genuine exchanges over scripted messages.
> - Need assistance? Call the **DevOps Team**.
> - Read the entire contribution document so that you can get involved in our workflow.
>

# Getting Started

To start developing this project, you need to set up your system properly. Follow these steps:

## Devbox

Devbox is a command-line tool that lets you easily create isolated shells for development. You start by defining the list of packages required for your project, and Devbox creates an isolated, reproducible environment with those packages installed.

>>>
🚨 Warning

Use this option if you don't want to install the tools at system level.
>>>

Follow these steps to configure your environment:

- Install [devbox](https://www.jetpack.io/devbox/docs/installing_devbox/).

```bash
curl -fsSL https://get.jetpack.io/devbox | bash
```

- Execute the following command to generate the temporary environment:

```bash
devbox shell
```

If you need more details about this configuration, check the [devbox.json](devbox.json) file. This setting is a global setting that we use. If you want to customize it, feel free.

## Direnv

Direnv is a powerful tool for managing environment variables and securely handling sensitive information such as API keys or credentials within a `.env` file.

>>>
🚨 Warning

In the `.envrc` file we configure it to load data from a `.env` file. Therefore, when you define your `.env`, `direnv` will load the variables from `.env` into your shell.
>>>

Follow these steps to configure your environment:

- Go to the [direnv](https://direnv.net/docs/installation.html) documentation and follow the instructions to install it.
- After installation, create an `.env` file in the root of your project.
- By default, `direnv` blocks itself from loading the contents of the `.envrc` file into your session as a security precaution when you are creating `.envrc` for the first time or whenever you modify the contents of the `.envrc` file. Run the following command to solve it.

```bash
direnv allow
```

By following these steps, you can utilize `direnv` to manage sensitive information securely and ensure that your environment variables are loaded safely whenever you work on your project.

If you need more details, check the [.envrc](.envrc) file.

## Task

The `task` tool provides a convenient way to define and manage project-specific tasks, making it easier to automate common scripts and simplifying development workflows.

>>>
🚨 Warning

We will use `task` instead of `make` for this project.
>>>

Follow these steps to configure your environment:

- Make sure you have installed the `task` command following the `devbox` configuration steps.
- Run the `task` command from the root directory of the project to see all the available commands.

If you need more details about each task defined, check the [Taskfile.yaml](Taskfile.yaml) file.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Commit Messages

In this project, we require that all commits follow a **specific** message format. This will make it easier for anyone who is contributing to the project to read the commit history and understand the changes that have been made. Here's how it works:

```
<type>(optional scope): <description>

[optional body]
```

`<type>`

This describes the type of change this commit provides.

- **feat**: A new feature (adding a new component, providing new variants for an existing component, etc.).
- **fix**: A bug fix (correcting a styling issue, addressing a bug in a component's API, etc.).
When updating non-dev dependencies, mark your changes with the `fix:` type.
- **docs**: Documentation-only changes.
- **style**: Changes that do not affect the meaning of the code
(whitespace, formatting, missing semicolons, etc). Not to be used for UI changes as those are
meaningful changes, consider using `feat:` of `fix:` instead.
- **refactor**: A code change that neither fixes a bug nor adds a feature.
- **perf**: A code change that improves performance.
- **test**: Adding missing tests or correcting existing tests.
- **build**: Changes that affect the build system.
- **ci**: Changes to our CI/CD configuration files and scripts.
- **chore**: Other changes that don't modify source or test files. Use this type when adding or
updating dev dependencies.
- **revert**: Reverts a previous commit.

Each commit `type` can have an optional `scope` to specify the place of the commit change. It is up to you to add or omit a commit's scope. When a commit affects a specific component, use the component's PascalCase name as the commit's scope. For example:

```
feat(login): add route
```

`<scope>`

Scope can be anything specifying the place of the commit change. For example `events`, `kafka`, `userModel`, `authorization`, etc.

`<description>`

This is a very short description of the change:

- Use the imperative, present tense: 'change' not 'changed' or 'changed'.
- Don't capitalize the first letter.
- No dot (.) at the end.

This format is based on [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). See the documentation for more details.

>
> [NOTE] 📌
> Each `type` of commit has an effect on the next version you release.
>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# MR Process

In general, it's a good idea to follow the conventions for the title of your MR, as well as for commit messages. That way, if your merge request gets squashed after the merge, the maintainer can use the title as the final commit message, creating a properly formatted history.

## Steps

- Create a branch from the trunk (`main`).
- Make the changes and commit.
- Make sure your code meets the quality standards.
- Submit your merge request!
- We typically enable the following Gitlab merge options
  - *Delete source branch if merge request is accepted*.
  - *Squash commits if merge request is accepted*.
- A maintainer will review your code and merge it.

>
> [NOTE] 📌
> If you have multiple commits in your PR, that solve the same problem, please **squash the commits**.
>

**Web interface**

- Set the title using Conventional Commits.
- Check Remove source branch if merge request is accepted.
- Check Squash commits if merge request is accepted.
- Merge only if:
  - Peer review has been done.
  - At least one thumbs up has been given.
  - All discussions have been resolved.

## Reviewing Process

- Be respectful and constructive.
- Check if all checks are passing.
- Suggest changes instead of simply commenting on found issues.
- If you are unsure about something, ask the author.
- If you are happy with the changes, approve the MR.
- Merge the MR once it has all approvals and the checks are passing.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Versioning Process

This project follows the [SemVer](https://semver.org/) specification. See the documentation for more details.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
<!-- END_DOCS -->
