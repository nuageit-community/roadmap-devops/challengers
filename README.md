<!-- BEGIN_DOCS -->
<div align="center">

<a name="readme-top"></a>

<img alt="gif-about" src=".gitlab/assets/challenge.gif" width="375"/>

<h1> NuageIT Tech-Challengers </h1>

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](https://www.conventionalcommits.org/en/v1.0.0/)
[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://semantic-release.gitbook.io/semantic-release/usage/configuration)
[![Built with Devbox](https://jetpack.io/img/devbox/shield_galaxy.svg)](https://jetpack.io/devbox/docs/contributor-quickstart/)

</div>

# Summary

[[_TOC_]]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Description

A NuageIT está sempre em busca de soluções inovadoras e de profissionais altamente capacitados para trabalhar em sua equipe de DevOps. Para isso, a empresa criou uma iniciativa que visa centralizar desafios técnicos em um repositório no GitLab, visando evoluir os profissionais já alocados e também encontrar talentos.

Este repositório tem como objetivo oferecer desafios técnicos que permitem aos profissionais testar suas habilidades e conhecimentos em diversas áreas, como Docker, Kubernetes, Helm, GitLab CI e outras tecnologias de infraestrutura modernas. Os desafios são estruturados para estimular a criatividade e a solução de problemas, além de promover a colaboração e a troca de conhecimentos entre os membros da equipe.

Os desafios propostos são projetados para serem realizados em um curto espaço de tempo, o que os torna adequados para o uso como uma atividade regular na rotina da equipe. Além disso, eles podem ser usados para avaliar o nível de habilidade e conhecimento dos profissionais de DevOps da NuageIT, permitindo a criação de planos de desenvolvimento pessoal e profissional para cada membro da equipe.

Essa iniciativa da NuageIT tem como objetivo criar uma cultura de aprendizagem contínua na empresa e incentivar a busca constante por soluções inovadoras e eficientes para os desafios do mercado. Além disso, a iniciativa visa fortalecer a equipe de DevOps da NuageIT, oferecendo a oportunidade de aprimorar suas habilidades e conhecimentos em tecnologias modernas de infraestrutura.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# References

If you have any reference links that you think are relevant to this project/documentation, please post them here:

- [Bregman - DevOps Exercises](https://github.com/bregman-arie/devops-exercises)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Versioning

To check the change history, please access the [**CHANGELOG.md**](CHANGELOG.md) file.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Troubleshooting

If you have any problems, please contact **Plataform Team**.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Show your support

<div align="center">

Give this project a ⭐️ if it helped you!

<br>

<img alt="gif-footer" src="https://gitlab.com/nuageit-community/images-toolbox/-/raw/main/.gitlab/assets/gif-footer.gif" width="225"/>

<br>
<br>

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)

<br>

Copyright © 2024 NuageIT

</div>

<p align="right">(<a href="#readme-top">back to top</a>)</p>
<!-- END_DOCS -->
